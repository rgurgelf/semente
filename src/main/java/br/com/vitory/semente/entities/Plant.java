package br.com.vitory.semente.entities;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Plant {

    @Id
    private Long id;
    private String name;
    private Double airHumidity;
    private Double airTemperature;
    private Double soilMoisture;
    private Double soilTemperature;

}
