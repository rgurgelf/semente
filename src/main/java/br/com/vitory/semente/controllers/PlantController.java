package br.com.vitory.semente.controllers;

import br.com.vitory.semente.entities.Plant;
import br.com.vitory.semente.services.PlantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

@RestController(value = "")
public class PlantController {

    @Autowired
    private PlantService plantService;

    @GetMapping
    public List<Plant> getAllPlants() {
        return plantService.findAll();
    }

    @GetMapping("/{id}")
    public Plant getPlant(@PathVariable int id) {
        return plantService.findById(id);
    }

    @PutMapping("/{id")
    public ResponseEntity<Void> persistReadings(@PathVariable int id, @RequestBody Plant plant) {
        plantService.save(plant);
        URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(plant.getId()).toUri();
        return ResponseEntity.created(uri).build();
    }

}
