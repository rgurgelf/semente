package br.com.vitory.semente.services;

import br.com.vitory.semente.entities.Plant;
import br.com.vitory.semente.repositories.PlantRepository;
import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PlantService {

    @Autowired
    private PlantRepository plantRepository;

    public List<Plant> findAll() {
        return plantRepository.findAll();
    }

    public Plant findById(int id) {
        return plantRepository.findById(id)
                .orElseThrow(() -> new ObjectNotFoundException("Object not found!", Plant.class));
    }

    public Plant save(Plant plant) {
        return plantRepository.save(plant);
    }

    public void delete(Plant plant) {
        plantRepository.delete(plant);
    }

}
